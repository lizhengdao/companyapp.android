package com.agrzybow.companyapp.android.postmanagement.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.postmanagement.model.PostEntity;
import com.agrzybow.companyapp.android.postmanagement.service.PostService;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostHolder> {
    List<PostEntity> entityList = new ArrayList<>();

    public PostAdapter(Context context) {
        new PostService(context).getPostList(formEntities -> {
            entityList = formEntities;
            notifyDataSetChanged();
        });
    }

    @NotNull
    @Override
    public PostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PostHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.post, parent, false));
    }

    @Override
    public void onBindViewHolder(PostHolder holder, int position) {
        holder.bindPost(entityList.get(position));
    }

    @Override
    public int getItemCount() {
        return entityList.size();
    }
}