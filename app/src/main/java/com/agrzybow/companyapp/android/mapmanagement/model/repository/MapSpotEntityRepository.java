package com.agrzybow.companyapp.android.mapmanagement.model.repository;

import android.content.Context;
import com.agrzybow.companyapp.android.general.util.DatabaseHelper;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotEntity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class MapSpotEntityRepository {
    private Dao<MapSpotEntity, Integer> dao;

    public MapSpotEntityRepository(Context context) {
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(MapSpotEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public MapSpotEntity getNewestEntity() throws SQLException {
        return dao.queryBuilder().orderBy(MapSpotEntity.DATE_FIELD_NAME, false).queryForFirst();
    }

    public List<MapSpotEntity> getAllEntities() throws SQLException {
        return dao.queryBuilder().orderBy(MapSpotEntity.DATE_FIELD_NAME, false).query();
    }

    public MapSpotEntity getEntityById(Integer id) throws SQLException {
        return dao.queryForId(id);
    }

    public void createOrUpdateEntity(MapSpotEntity mapSpotEntity) throws SQLException {
        dao.createOrUpdate(mapSpotEntity);
    }

    public void createOrUpdateEntities(List<MapSpotEntity> formEntities) throws SQLException {
        for (MapSpotEntity mapSpotEntity : formEntities) {
            createOrUpdateEntity(mapSpotEntity);
        }
    }
}
