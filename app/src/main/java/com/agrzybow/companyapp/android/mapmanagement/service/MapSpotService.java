package com.agrzybow.companyapp.android.mapmanagement.service;

import android.content.Context;
import android.widget.Toast;
import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.general.service.RetrofitClientService;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotEntity;
import com.agrzybow.companyapp.android.mapmanagement.model.api.MapSpotAPI;
import com.agrzybow.companyapp.android.mapmanagement.model.repository.MapSpotEntityRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MapSpotService {
    private MapSpotAPI mapSpotAPI;
    private MapSpotEntityRepository mapSpotEntityRepository;

    private Context context;
    private List<Disposable> disposableList = new ArrayList<>();

    public MapSpotService(Context context) {
        this.context = context;
        mapSpotAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(MapSpotAPI.class);
        mapSpotEntityRepository = new MapSpotEntityRepository(context);
    }

    public void getMapSpotList(Consumer<List<MapSpotEntity>> consumer) {
        Long lastEntityDate = null;
        try {
            MapSpotEntity newestMapSpotEntity = mapSpotEntityRepository.getNewestEntity();
            if (newestMapSpotEntity != null) {
                lastEntityDate = newestMapSpotEntity.getModifyDate();
            }
        } catch (SQLException ignored) {
        }

        disposableList.add(mapSpotAPI.getAllEntities(lastEntityDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mapSpotEntities -> {
                    mapSpotEntityRepository.createOrUpdateEntities(mapSpotEntities);
                    consumer.accept(mapSpotEntityRepository.getAllEntities());
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                    consumer.accept(mapSpotEntityRepository.getAllEntities());
                }));
    }

    public void saveNewMapSpot(String username, MapSpotEntity newMapSpotEntity, Consumer<MapSpotEntity> consumer, Context context) {
        disposableList.add(mapSpotAPI.saveEntity(username, newMapSpotEntity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mapSpotEntity -> {
                    mapSpotEntityRepository.createOrUpdateEntity(mapSpotEntity);
                    consumer.accept(mapSpotEntity);
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                }));
    }
}
