package com.agrzybow.companyapp.android.general.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Klasa zawierająca wszystkie informacje na temat użytkownika
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "User")
public class UserEntity implements Serializable {
    public static final String USERNAME_FIELD_NAME = "username";
    public static final String NAME_FIELD_NAME = "name";
    public static final String EMAIL_FIELD_NAME = "email";
    public static final String PERMISSION_FIELD_NAME = "userPermission";
    public static final String GROUP_ID_FIELD_NAME = "userGroup";
    public static final String PHONE_NUMBER_FIELD_NAME = "phoneNumber";
    public static final String DATE_FIELD_NAME = "modifyDate";
    public static final String MODIFY_BY_FIELD_NAME = "modifyBy";

    /** Unikalna nazwa użytkownika (np. j.kowalski) */
    @NotNull
    @Size(min = 1)
    @Id
    @Column(name = USERNAME_FIELD_NAME, nullable = false)
    private String username;

    /** Pełna nazwa użytkownika (np. Jan Kowalski) */
    @NotNull
    @Size(min = 1)
    @Column(name = NAME_FIELD_NAME, nullable = false)
    private String name;

    /** Adres e-mail */
    @NotNull
    @Email
    @Size(min = 1)
    @Column(name = EMAIL_FIELD_NAME)
    private String email;

    /** Numer telefonu służbowego */
    @NotNull
    @Size(min = 1)
    @Column(name = PHONE_NUMBER_FIELD_NAME, nullable = false)
    private String phoneNumber;

    /** Obiekt grupy, do której należy użytkownik */
    @NotNull
    @ManyToOne
    @JoinColumn(name = GROUP_ID_FIELD_NAME, nullable = false)
    private UserGroupEntity userGroup;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;
}
