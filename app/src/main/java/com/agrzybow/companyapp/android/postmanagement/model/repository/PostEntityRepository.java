package com.agrzybow.companyapp.android.postmanagement.model.repository;

import android.content.Context;
import com.agrzybow.companyapp.android.general.util.DatabaseHelper;
import com.agrzybow.companyapp.android.postmanagement.model.PostEntity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class PostEntityRepository {
    private Dao<PostEntity, Integer> dao;

    public PostEntityRepository(Context context) {
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(PostEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PostEntity getNewestEntity() throws SQLException {
        return dao.queryBuilder().orderBy(PostEntity.DATE_FIELD_NAME, false).queryForFirst();
    }

    public List<PostEntity> getAllEntities() throws SQLException {
        return dao.queryBuilder().orderBy(PostEntity.DATE_FIELD_NAME, false).query();
    }

    public PostEntity getEntityById(Integer id) throws SQLException {
        return dao.queryForId(id);
    }

    public void createOrUpdateEntity(PostEntity postEntity) throws SQLException {
        dao.createOrUpdate(postEntity);
    }

    public void createOrUpdateEntities(List<PostEntity> postEntities) throws SQLException {
        for (PostEntity data : postEntities) {
            createOrUpdateEntity(data);
        }
    }
}
