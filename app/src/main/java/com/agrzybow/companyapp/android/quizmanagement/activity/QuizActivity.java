package com.agrzybow.companyapp.android.quizmanagement.activity;


import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.quizmanagement.adapter.QuizAnswerAdapter;
import com.agrzybow.companyapp.android.quizmanagement.model.dto.QuizDTO;
import com.agrzybow.companyapp.android.quizmanagement.service.QuizService;

import java.util.List;


public class QuizActivity extends AppCompatActivity {
    public static final int ACTIVITY_CODE = 2;

    private LinearLayout hintContainer;
    private ProgressBar timeProgressBar;

    private QuizService quizService;
    private List<QuizDTO> quizDTOList;
    private int currentQuizIndex = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        quizService = new QuizService(getApplicationContext());
        quizService.getQuiz(quizDTOS -> {
            if(quizDTOS.size() > 0) {
                this.setContentView(R.layout.activity_quiz);
                hintContainer = findViewById(R.id.quiz_hint_container);
                timeProgressBar = findViewById(R.id.timer_to_change);

                quizDTOList = quizDTOS;
                initializeQuiz(quizDTOList.get(currentQuizIndex));
            } else {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    private void initializeQuiz(QuizDTO quizDTO) {
        hintContainer.setVisibility(View.INVISIBLE);
        timeProgressBar.setVisibility(View.INVISIBLE);

        ((TextView) findViewById(R.id.quiz_hint_text_view)).setText(quizDTO.getQuestion().getHint());
        ((TextView) findViewById(R.id.question_container)).setText(quizDTO.getQuestion().getQuestionText());

        RecyclerView answersRecyclerView = findViewById(R.id.answers_recycler_view);
        answersRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        answersRecyclerView.setAdapter(new QuizAnswerAdapter(this, quizDTO, () -> {
            if (++currentQuizIndex < quizDTOList.size()) {
                initializeQuiz(quizDTOList.get(currentQuizIndex));
            } else {
                setResult(RESULT_OK);
                finish();
            }
        }));
    }

    private String getClientUsername() {
        return PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(UserService.CLIENT_USERNAME, null);
    }
}
