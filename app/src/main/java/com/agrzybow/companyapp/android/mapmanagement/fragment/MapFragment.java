package com.agrzybow.companyapp.android.mapmanagement.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.agrzybow.R;
import com.agrzybow.companyapp.android.mapmanagement.activity.NewMapSpotFormActivity;
import com.agrzybow.companyapp.android.mapmanagement.adapter.MapSpotInfoWindowAdapter;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotEntity;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotCategoryEntity;
import com.agrzybow.companyapp.android.mapmanagement.service.MapSpotCategoryService;
import com.agrzybow.companyapp.android.mapmanagement.service.MapSpotService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.*;
import java.util.stream.Collectors;

public class MapFragment extends Fragment implements OnMapReadyCallback {
    private final int LOCATION_PERMISSION_REQUEST_CODE = 9749;
    private static final float DEFAULT_ZOOM = 15f;

    private boolean locationPermissionsGranted = false;

    private GoogleMap mMap;

    private List<Marker> mapSpotMarkers = new ArrayList<>();
    private Map<Integer, MapSpotCategoryEntity> mapSpotCategoryEntityMap;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLocationPermission();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NewMapSpotFormActivity.ACTIVITY_CODE && resultCode == NewMapSpotFormActivity.RESULT_OK) {
            MapSpotEntity newSpot = (MapSpotEntity) data.getSerializableExtra(NewMapSpotFormActivity.NEW_SPOT_KEY);
            if (newSpot != null) {
                byte[] iconBytes = Base64.decode(newSpot.getCategory().getImage(), Base64.DEFAULT);
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(newSpot.getLat(), newSpot.getLng()))
                        .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeByteArray(iconBytes, 0, iconBytes.length)))
                        .anchor(0.5f, 1));
                marker.setTag(newSpot);
                mapSpotMarkers.add(marker);
            }
        }
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        moveCameraToCurrentLocation();

        ImageView filterImageView = Objects.requireNonNull(getActivity()).findViewById(R.id.ic_filter);
        ImageView newSpotImageView = getActivity().findViewById(R.id.ic_new_spot);

        new MapSpotCategoryService(getContext()).getMapSpotCategoryList(mapSpotCategories -> {
            mapSpotCategoryEntityMap = mapSpotCategories.stream().collect(Collectors.toMap(MapSpotCategoryEntity::getId, value -> value));
            new MapSpotService(getContext()).getMapSpotList(mapSpotEntities -> {
                putMarkersOnTheMap(mapSpotEntities);
                AlertDialog filterDialog = createFilterDialog(getContext());

                filterImageView.setOnClickListener(v -> filterDialog.show());
                newSpotImageView.setOnClickListener(v -> {
                    Intent x = new Intent(getContext(), NewMapSpotFormActivity.class);
                    startActivityForResult(x, NewMapSpotFormActivity.ACTIVITY_CODE);
                });
            });
        });

    }

    public AlertDialog createFilterDialog(Context context) {
        Integer[] categoryIds = mapSpotCategoryEntityMap.values().stream().map(MapSpotCategoryEntity::getId).toArray(Integer[]::new);
        boolean[] checkedCategories = new boolean[categoryIds.length];
        Arrays.fill(checkedCategories, true);

        return new AlertDialog.Builder(context)
                .setTitle(R.string.filter_dialog)
                .setMultiChoiceItems(mapSpotCategoryEntityMap.values().stream().map(MapSpotCategoryEntity::getName).toArray(CharSequence[]::new), checkedCategories, (dialog, which, isChecked) -> {
                    checkedCategories[which] = isChecked;
                })
                .setPositiveButton(R.string.filterCategoryPositiveButton, (dialog, which) -> mapSpotMarkers.forEach(marker -> {
                    MapSpotEntity mapSpotEntity = (MapSpotEntity) marker.getTag();
                    for (int i = 0; i < categoryIds.length; i++) {
                        if (categoryIds[i].equals(mapSpotEntity.getCategory().getId())) {
                            if (checkedCategories[i]) {
                                marker.setVisible(true);
                            } else {
                                marker.setVisible(false);
                            }
                        }
                    }
                })).create();
    }

    private void putMarkersOnTheMap(List<MapSpotEntity> mapSpotEntities) {
        mapSpotEntities.forEach(mapSpotEntity -> {
            MapSpotCategoryEntity mapSpotCategoryEntity = mapSpotCategoryEntityMap.get(mapSpotEntity.getCategory().getId());
            byte[] iconBytes = Base64.decode(mapSpotCategoryEntity.getImage(), Base64.DEFAULT);
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(mapSpotEntity.getLat(), mapSpotEntity.getLng()))
                    .icon(BitmapDescriptorFactory
                            .fromBitmap(BitmapFactory
                                    .decodeByteArray(iconBytes, 0, iconBytes.length)))
                    .anchor(0.5f, 1));
            marker.setTag(mapSpotEntity);
            mapSpotMarkers.add(marker);
        });
        mMap.setInfoWindowAdapter(new MapSpotInfoWindowAdapter(getContext(), mapSpotCategoryEntityMap));
    }

    private void moveCameraToCurrentLocation() {
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getContext()));
        try {
            if (locationPermissionsGranted) {
                fusedLocationProviderClient.getLastLocation().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Location currentLocation = task.getResult();
                        if (currentLocation != null) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM));
                        } else {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.661229, 16.100957), DEFAULT_ZOOM));
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLocationPermission() {
        String[] permissions = {
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        requestPermissions(permissions, LOCATION_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        locationPermissionsGranted = true;
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    locationPermissionsGranted = false;
                    break;
                }
            }
            if (locationPermissionsGranted) {
                initMap();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMap.clear();
    }
}
