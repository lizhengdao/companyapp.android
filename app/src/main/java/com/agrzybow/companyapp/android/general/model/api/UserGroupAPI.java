package com.agrzybow.companyapp.android.general.model.api;

import com.agrzybow.companyapp.android.general.model.UserGroupEntity;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface UserGroupAPI {
    @GET("user-group/display")
    Observable<List<UserGroupEntity>> getAllEntities(@Query("newerThan") Long newerThan);
}
