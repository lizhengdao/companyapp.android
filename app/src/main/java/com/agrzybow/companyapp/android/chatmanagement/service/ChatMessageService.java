package com.agrzybow.companyapp.android.chatmanagement.service;

import android.content.Context;
import android.widget.Toast;
import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.android.chatmanagement.model.api.ChatMessageAPI;
import com.agrzybow.companyapp.android.chatmanagement.model.repository.ChatMessageEntityRepository;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.service.RetrofitClientService;
import com.agrzybow.companyapp.android.general.service.UserService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;

public class ChatMessageService {
    private ChatMessageAPI chatMessageAPI;
    private ChatMessageEntityRepository chatMessageEntityRepository;
    private Context context;
    private String username;

    private List<Disposable> disposableList = new ArrayList<>();

    public ChatMessageService(Context context) {
        this.context = context;
        this.username = PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_USERNAME, null);
        this.chatMessageAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(ChatMessageAPI.class);
        this.chatMessageEntityRepository = new ChatMessageEntityRepository(context);
    }

    public void saveEntity(ChatMessageEntity entityToSave, Consumer<ChatMessageEntity> consumer) {
        disposableList.add(chatMessageAPI.saveEntity(username, entityToSave)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    response.setRead(true);
                    chatMessageEntityRepository.createOrUpdateEntity(response);
                    consumer.accept(response);
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                }));
    }

    public void getAllEntitiesForContact(UserEntity contact, Consumer<List<ChatMessageEntity>> consumer) {
        try {
            consumer.accept(chatMessageEntityRepository.getAllEntities(contact));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
