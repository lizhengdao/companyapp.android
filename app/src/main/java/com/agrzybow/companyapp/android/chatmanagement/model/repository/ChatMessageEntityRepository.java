package com.agrzybow.companyapp.android.chatmanagement.model.repository;

import android.content.Context;
import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.util.DatabaseHelper;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class ChatMessageEntityRepository {
    private Dao<ChatMessageEntity, Integer> dao;

    public ChatMessageEntityRepository(Context context) {
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(ChatMessageEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ChatMessageEntity getNewestEntity() throws SQLException {
        return dao.queryBuilder().orderBy(ChatMessageEntity.DATE_FIELD_NAME, false).queryForFirst();
    }

    public ChatMessageEntity getNewestEntityFromConversationWith(UserEntity userEntity) throws SQLException {
        return dao.queryBuilder().orderBy(ChatMessageEntity.DATE_FIELD_NAME, false).where().eq(ChatMessageEntity.USER_RECEIVER_FIELD_NAME, userEntity).or().eq(ChatMessageEntity.MODIFY_BY_FIELD_NAME, userEntity).queryForFirst();
    }

    public List<ChatMessageEntity> getAllEntities(UserEntity userEntity) throws SQLException {
        return dao.queryBuilder().orderBy(ChatMessageEntity.DATE_FIELD_NAME, true).where().eq(ChatMessageEntity.USER_RECEIVER_FIELD_NAME, userEntity).or().eq(ChatMessageEntity.MODIFY_BY_FIELD_NAME, userEntity).query();
    }

    public void createOrUpdateEntity(ChatMessageEntity chatMessageEntity) throws SQLException {
        dao.createOrUpdate(chatMessageEntity);
    }

    public void createOrUpdateEntities(List<ChatMessageEntity> chatMessageEntities) throws SQLException {
        for (ChatMessageEntity chatMessageEntity : chatMessageEntities) {
            createOrUpdateEntity(chatMessageEntity);
        }
    }
}
