package com.agrzybow.companyapp.android.formmanagement.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.formmanagement.activity.FormActivity;
import com.agrzybow.companyapp.android.formmanagement.model.FormEntity;

public class FormHolder extends RecyclerView.ViewHolder {
    private TextView formNameTextView;

    public FormHolder(@NonNull View itemView) {
        super(itemView);
        formNameTextView = itemView.findViewById(R.id.form_name_text_view);
    }

    public void bindForm(FormEntity formEntity) {
        formNameTextView.setText(formEntity.getName());
        itemView.setOnClickListener(e -> {
            Intent intent = new Intent(e.getContext(), FormActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(FormActivity.FORM_ENTITY, formEntity);
            intent.putExtras(bundle);
            e.getContext().startActivity(intent);
        });
    }
}
