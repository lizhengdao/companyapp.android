package com.agrzybow.companyapp.android.mapmanagement.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotEntity;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotCategoryEntity;
import com.agrzybow.companyapp.android.mapmanagement.service.MapSpotCategoryService;
import com.agrzybow.companyapp.android.mapmanagement.service.MapSpotService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class NewMapSpotFormActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final int ACTIVITY_CODE = 10;
    public static final String NEW_SPOT_KEY = "NEW_SPOT_TO_ADD_ON_MAP";
    private static final float DEFAULT_ZOOM = 15f;

    private GoogleMap mMap;
    private Marker positionMarker;
    private EditText nameEditText;
    private EditText descriptionEditText;

    private MapSpotService mapSpotService;
    private List<MapSpotCategoryEntity> mapSpotCategoryEntities;
    private MapSpotCategoryEntity selectedCategory;

    private String username;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_spot);
        nameEditText = findViewById(R.id.map_spot_form_name);
        descriptionEditText = findViewById(R.id.map_spot_form_description);
        username = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(UserService.CLIENT_USERNAME, null);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_spot_form_map);
        mapFragment.getMapAsync(this);

        mapSpotService = new MapSpotService(getApplicationContext());

        new MapSpotCategoryService(getApplicationContext()).getMapSpotCategoryList(mapSpotCategories -> {
            this.mapSpotCategoryEntities = mapSpotCategories;
            if (mapSpotCategories.size() > 0) {
                selectedCategory = mapSpotCategories.get(0);
            } else {
                Toast.makeText(getApplicationContext(), getResources().getText(R.string.new_spot_category_null_error), Toast.LENGTH_LONG).show();
                setResult(RESULT_CANCELED);
                finish();
            }

            Spinner spinner = findViewById(R.id.map_spot_form_category_spinner);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedCategory = mapSpotCategoryEntities.get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, mapSpotCategoryEntities.stream().map(MapSpotCategoryEntity::getName).collect(Collectors.toList()));
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(0, 0));
        positionMarker = mMap.addMarker(markerOptions);

        mMap.setOnCameraMoveListener(() -> positionMarker.setPosition(mMap.getCameraPosition().target));
        moveCameraToCurrentLocation();
    }

    private void moveCameraToCurrentLocation() {
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getApplicationContext()));
        try {
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Location currentLocation = task.getResult();
                    if (currentLocation != null) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM));
                    } else {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.661229, 16.100957), DEFAULT_ZOOM));
                    }
                }
            });
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void onAddButton(View view) {
        MapSpotEntity newMapSpotEntity = MapSpotEntity.builder()
                .lat(positionMarker.getPosition().latitude)
                .lng(positionMarker.getPosition().longitude)
                .name(nameEditText.getText().toString())
                .description(descriptionEditText.getText().toString())
                .verified(false)
                .category(selectedCategory)
                .modifyDate(System.currentTimeMillis())
                .modifyBy(UserEntity.builder()
                        .username(username)
                        .build())
                .build();

        mapSpotService.saveNewMapSpot(username, newMapSpotEntity, mapSpot -> {
            Intent resultIntent = new Intent();
            resultIntent.putExtra(NEW_SPOT_KEY, newMapSpotEntity);
            setResult(RESULT_OK, resultIntent);
            finish();
        }, getApplicationContext());
    }
}
