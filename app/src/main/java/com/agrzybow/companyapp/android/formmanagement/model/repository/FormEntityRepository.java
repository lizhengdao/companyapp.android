package com.agrzybow.companyapp.android.formmanagement.model.repository;

import android.content.Context;
import com.agrzybow.companyapp.android.formmanagement.model.FormEntity;
import com.agrzybow.companyapp.android.general.util.DatabaseHelper;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class FormEntityRepository {
    private Dao<FormEntity, Integer> dao;

    public FormEntityRepository(Context context) {
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(FormEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public FormEntity getNewestEntity() throws SQLException {
        return dao.queryBuilder().orderBy(FormEntity.DATE_FIELD_NAME, false).queryForFirst();
    }

    public List<FormEntity> getAllEntities() throws SQLException {
        return dao.queryBuilder().orderBy(FormEntity.DATE_FIELD_NAME, false).query();
    }

    public FormEntity getEntityById(Integer id) throws SQLException {
        return dao.queryForId(id);
    }

    public void createOrUpdateEntity(FormEntity formEntity) throws SQLException {
        dao.createOrUpdate(formEntity);
    }

    public void createOrUpdateEntities(List<FormEntity> formEntities) throws SQLException {
        for (FormEntity formEntity : formEntities) {
            createOrUpdateEntity(formEntity);
        }
    }
}
