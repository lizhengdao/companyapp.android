package com.agrzybow.companyapp.android.general.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.general.service.UserService;
import okhttp3.Credentials;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Aktywność obsługująca zmianę hasła użytkownika
 */
public class ResetPasswordActivity extends AppCompatActivity {
    public static final int ACTIVITY_CODE = 1;

    private UserService userService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.userService = new UserService(getApplicationContext(), getClientUsername());
        userService.checkResetPassword(response -> {
            if(response) {
                this.setTitle(getResources().getString(R.string.reset_password));
                this.setContentView(R.layout.activity_reset_password);
            } else {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    public void confirmPasswordChange(View view) {
        TextView passwordTextView = findViewById(R.id.new_password);
        TextView confirmPasswordTextView = findViewById(R.id.new_password_confirm);

        final String password = passwordTextView.getText().toString();
        String confirmPassword = confirmPasswordTextView.getText().toString();

        if (password.length() > 0 && password.equals(confirmPassword)) {
            userService.changePassword(new BCryptPasswordEncoder().encode(password), result -> {
                String newCredentials = Credentials.basic(getClientUsername(), password);
                setNewCredentials(newCredentials);
                setResult(RESULT_OK);
                finish();
            });
        } else {
            Toast.makeText(getApplicationContext(), R.string.error_new_password, Toast.LENGTH_LONG).show();
        }
    }

    private String getClientUsername() {
        return PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(UserService.CLIENT_USERNAME, null);
    }

    private void setNewCredentials(String credentials) {
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(UserService.CLIENT_CREDENTIALS, credentials).apply();
    }

    /**
     * Metoda obsługująca działanie przycisku wylogowania
     *
     * @param view Obiekt przycisku
     */
    public void logout(View view) {
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
                .remove(UserService.CLIENT_USERNAME)
                .remove(UserService.CLIENT_CREDENTIALS)
                .remove(UserService.CLIENT_ENTITY)
                .apply();

        setResult(RESULT_CANCELED);
        finish();
    }
}
