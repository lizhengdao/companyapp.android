package com.agrzybow.companyapp.android.mapmanagement.model.api;

import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotCategoryEntity;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface MapSpotCategoryAPI {
    @GET("map-spot-category/display")
    Observable<List<MapSpotCategoryEntity>> getAllEntities(@Query("newerThan") Long newerThan);
}
