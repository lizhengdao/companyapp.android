package com.agrzybow.companyapp.android.mapmanagement.model;

import com.agrzybow.companyapp.android.general.model.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "MapSpot")
public class MapSpotEntity implements Serializable {
    public static final String ID_FIELD_NAME = "id";
    public static final String LAT_FIELD_NAME = "lat";
    public static final String LNG_FIELD_NAME = "lng";
    public static final String NAME_FIELD_NAME = "name";
    public static final String DESCRIPTION_FIELD_NAME = "description";
    public static final String VERIFIED_FIELD_NAME = "verified";
    public static final String CATEGORY_FIELD_NAME = "category";
    public static final String DATE_FIELD_NAME = "modifyDate";
    public static final String MODIFY_BY_FIELD_NAME = "modifyBy";

    @Id
    @Column(name = ID_FIELD_NAME)
    private Integer id;

    @NotNull
    @Min(-90)
    @Max(90)
    @Column(name = LAT_FIELD_NAME, nullable = false)
    private Double lat;

    @NotNull
    @Min(-180)
    @Max(180)
    @Column(name = LNG_FIELD_NAME, nullable = false)
    private Double lng;

    @NotNull
    @Size(min = 1)
    @Column(name = NAME_FIELD_NAME, nullable = false)
    private String name;

    @NotNull
    @Size(min = 1)
    @Column(name = DESCRIPTION_FIELD_NAME, nullable = false, columnDefinition = "TEXT")
    private String description;

    @NotNull
    @Column(name = VERIFIED_FIELD_NAME, nullable = false)
    private Boolean verified;

    @NotNull
    @ManyToOne
    @JoinColumn(name = CATEGORY_FIELD_NAME, nullable = false)
    private MapSpotCategoryEntity category;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;

    @ManyToOne
    @JoinColumn(name = MODIFY_BY_FIELD_NAME)
    private UserEntity modifyBy;
}
