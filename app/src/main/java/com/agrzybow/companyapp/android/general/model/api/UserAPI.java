package com.agrzybow.companyapp.android.general.model.api;

import com.agrzybow.companyapp.android.general.model.UserEntity;
import io.reactivex.Observable;
import retrofit2.http.*;

import java.util.List;

public interface UserAPI {
    @GET("user/display/{username}")
    Observable<List<UserEntity>> getAllEntities(@Path("username") String username, @Query("newerThan") Long newerThan);
    @GET("user/display/{username}/login")
    Observable<UserEntity> login(@Path("username") String username);
    @GET("user/display/{username}/password-expired")
    Observable<Boolean> checkIfPasswordExpired(@Path("username") String username);
    @POST("user/display/{username}/change-password")
    Observable<Boolean> changePassword(@Path("username") String username, @Query("newPassword") String newPassword);
    @POST("user/add/{username}/save-token")
    Observable<String> saveToken(@Path("username") String username, @Query("token") String token);
}
