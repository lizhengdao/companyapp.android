package com.agrzybow.companyapp.android.inboxmanagement.model;

import com.agrzybow.companyapp.android.general.model.UserEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@Entity
@Table(name = "InboxItem")
public class InboxItemEntity {
    public static final String ID_FIELD_NAME = "id";
    public static final String TITLE_FIELD_NAME = "title";
    public static final String MESSAGE_TEXT_FIELD_NAME = "messageText";
    public static final String READ_BY_RECEIVER_FIELD_NAME = "readByReceiver";
    public static final String DATE_FIELD_NAME = "modifyDate";
    public static final String MODIFY_BY_FIELD_NAME = "modifyBy";

    @Id
    @Column(name = ID_FIELD_NAME)
    private Integer id;

    @NotNull
    @Size(min = 1)
    @Column(name = TITLE_FIELD_NAME, nullable = false)
    private String title;

    @NotNull
    @Size(min = 1)
    @Column(name = MESSAGE_TEXT_FIELD_NAME, nullable = false, columnDefinition = "TEXT")
    private String messageText;

    @NotNull
    @Column(name = READ_BY_RECEIVER_FIELD_NAME, nullable = false)
    private Boolean readByReceiver;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;

    @ManyToOne
    @JoinColumn(name = MODIFY_BY_FIELD_NAME)
    private UserEntity modifyBy;
}
