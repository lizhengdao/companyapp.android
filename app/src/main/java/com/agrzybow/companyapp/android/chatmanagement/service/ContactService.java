package com.agrzybow.companyapp.android.chatmanagement.service;

import android.content.Context;
import android.widget.Toast;
import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.android.chatmanagement.model.api.ChatMessageAPI;
import com.agrzybow.companyapp.android.chatmanagement.model.repository.ChatMessageEntityRepository;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.model.UserGroupEntity;
import com.agrzybow.companyapp.android.general.model.api.UserAPI;
import com.agrzybow.companyapp.android.general.model.api.UserGroupAPI;
import com.agrzybow.companyapp.android.general.model.repository.UserEntityRepository;
import com.agrzybow.companyapp.android.general.model.repository.UserGroupEntityRepository;
import com.agrzybow.companyapp.android.general.service.RetrofitClientService;
import com.agrzybow.companyapp.android.general.service.UserService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ContactService {
    private UserAPI userAPI;
    private UserGroupAPI userGroupAPI;
    private ChatMessageAPI chatMessageAPI;
    private UserEntityRepository userEntityRepository;
    private UserGroupEntityRepository userGroupEntityRepository;
    private ChatMessageEntityRepository chatMessageEntityRepository;

    private Context context;
    private String username;
    private List<Disposable> disposableList = new ArrayList<>();

    public ContactService(Context context) {
        this.context = context;
        this.username = PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_USERNAME, null);
        userAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(UserAPI.class);
        userGroupAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(UserGroupAPI.class);
        chatMessageAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(ChatMessageAPI.class);
        userEntityRepository = new UserEntityRepository(context);
        chatMessageEntityRepository = new ChatMessageEntityRepository(context);
        userGroupEntityRepository = new UserGroupEntityRepository(context);
    }

    public void getUserList(Consumer<List<UserEntity>> consumer) {
        Long lastEntityDate = null;
        try {
            UserEntity newestUserEntity = userEntityRepository.getNewestEntity();
            if (newestUserEntity != null) {
                lastEntityDate = newestUserEntity.getModifyDate();
            }
        } catch (SQLException ignored) {
        }

        disposableList.add(userAPI.getAllEntities(username, lastEntityDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userEntities -> {
                    userEntityRepository.createOrUpdateEntities(userEntities);
                    consumer.accept(userEntityRepository.getAllEntities());
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                    consumer.accept(userEntityRepository.getAllEntities());
                }));
    }

    public void synchronizeChatMessagesWithDatabase(Consumer<List<ChatMessageEntity>> consumer) {
        Long lastEntityDate = null;
        try {
            ChatMessageEntity newestChatMessageEntity = chatMessageEntityRepository.getNewestEntity();
            if (newestChatMessageEntity != null) {
                lastEntityDate = newestChatMessageEntity.getModifyDate();
            }
        } catch (SQLException ignored) {
        }

        disposableList.add(chatMessageAPI.getChatMessageEntityList(username, lastEntityDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(chatMessageEntities -> {
                    chatMessageEntities.forEach(chatMessageEntity -> {
                        chatMessageEntity.setRead(false);
                    });
                    chatMessageEntityRepository.createOrUpdateEntities(chatMessageEntities);
                    consumer.accept(chatMessageEntities);
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                    consumer.accept(null);
                })
        );
    }

    public void getLastMessages(Consumer<HashMap<String, ChatMessageEntity>> consumer) {
        try {
            List<UserEntity> userEntityList = userEntityRepository.getAllEntities();
            HashMap<String, ChatMessageEntity> result = new HashMap<>();
            userEntityList.forEach(userEntity -> {
                try {
                    result.put(userEntity.getUsername(), chatMessageEntityRepository.getNewestEntityFromConversationWith(userEntity));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            consumer.accept(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getUserGroupList(Consumer<List<UserGroupEntity>> consumer) {
        Long lastEntityDate = null;
        try {
            UserGroupEntity newestUserGroupEntity = userGroupEntityRepository.getNewestEntity();
            if (newestUserGroupEntity != null) {
                lastEntityDate = newestUserGroupEntity.getModifyDate();
            }
        } catch (SQLException ignored) {
        }

        disposableList.add(userGroupAPI.getAllEntities(lastEntityDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userGroupEntities -> {
                    userGroupEntityRepository.createOrUpdateEntities(userGroupEntities);
                    consumer.accept(userGroupEntityRepository.getAllEntities());
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                    consumer.accept(userGroupEntityRepository.getAllEntities());
                }));
    }
}
